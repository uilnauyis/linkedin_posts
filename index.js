const puppeteer = require('puppeteer');
const mkdirp = require('async-mkdirp');
const fsWritefilePromise = require('fs-writefile-promise');

const keywords = require('./configs/keywords.json');
const companiesAndUrls = require('./configs/companiesAndUrls.json');

let browser;
let loggedInPage;
let image_counter = 0;

async function run() {
	await mkdirp('data');
	console.log('Creates folder \'data\'');
	await mkdirp('screenshots');
	console.log('Creates folder \'screenshots\'');
	await startBrowser();
	await loginlinkedin();
	await saveRelevantPosts();
	await searchRelevantPostsStrategy2()
}

async function startBrowser() {
	browser = await puppeteer.launch({headless: true});
	console.log('Browser is started.');
}

async function loginlinkedin() {
	let page = await browser.newPage();
	await page.goto('https://www.linkedin.com/uas/login?_l=en');
	await page.type('#session_key-login', 'steven.lewistiva@gmail.com');
	await page.type('#session_password-login', '2oo8062E');
	await page.click('[name="signin"]');
	await page.waitForNavigation();
	await page.screenshot({path: 'screenshots/linkedin_login.png'});
	console.log('Login succeeded');
	loggedInPage = page;
}

async function saveRelevantPosts() {
	for (let i = 0; i < companiesAndUrls.length; i++) {
		const companyName = companiesAndUrls[i][0];
		const urls = companiesAndUrls[i].slice(1);
		let relevantPosts = [];

		for (let j = 0; j < urls.length; j++) {
			await resolveAfter10Seconds();
			await loggedInPage.goto(urls[j]);
			await lazyLoad();

			const posts = await loggedInPage.evaluate(function() {
				const countainers = Array.from(document.querySelectorAll('#organization-feed .feed-shared-text-view'));
				return countainers.map(item => item.innerText);
			});
			console.log(posts);

			const filteredPosts = filter(posts);
			relevantPosts.push.apply(relevantPosts, filteredPosts);
		}

		savePosts(relevantPosts, companyName);
	}
}

async function searchRelevantPostsStrategy2() {
	const url = 'https://www.linkedin.com/search/results/content/v2/?keywords=%22verizon%20connect%22&origin=GLOBAL_SEARCH_HEADER';
	await resolveAfter10Seconds();
	await loggedInPage.goto(url);
	await lazyLoad();

	let relevantPosts = [];
	const posts = await loggedInPage.evaluate(function() {
		const countainers = Array.from(document.querySelectorAll('#voyager-feed .feed-shared-update-v2__description.feed-shared-inline-show-more-text.ember-view'));
		return countainers.map(item => item.innerText);
	});
	const posterInfo = await loggedInPage.evaluate(function() {
		const countainers = Array.from(document.querySelectorAll('#voyager-feed .display-flex.feed-shared-actor.display-flex.feed-shared-actor--with-control-menu.ember-view'));
		return countainers.map(item => item.innerText);
	});
	if (posterInfo.length != posts.length) {
		throw new Error('Unexpected situation: posterInfo.length != posts.length')
	}

	for (let i = 0; i < posts.length; i++) {
		const posterInfoInLowerCase = posterInfo[i].toLowerCase();
		if (posterInfoInLowerCase.indexOf('verizon') >= 0
			|| posts[i].length < 20)
			continue;
		relevantPosts.push(posts[i]);
	}

	savePosts(relevantPosts, "all-verizon-connect-relevant");
}

function resolveAfter10Seconds() {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve();
		}, 2000);
	});
}

async function lazyLoad() {
	previousHeight = 0;
	while (previousHeight + 10 < await loggedInPage.evaluate('document.body.scrollHeight')) {
		previousHeight = await loggedInPage.evaluate('document.body.scrollHeight');
		await loggedInPage.evaluate('window.scrollTo(0, document.body.scrollHeight - 100)');
		await loggedInPage.waitFor(2000);
		await loggedInPage.screenshot({path: 'screenshots/' + (image_counter++) + '.png'});
	}
}

function filter(posts) {
	return posts.filter(post => {
		const postInLowerCase = post.toLowerCase();
		for (let i = 0;  i < keywords.length; i++) {
			if (postInLowerCase.indexOf(keywords[i]) > -1) {
				return true;
			}
		}
		return false;
	});
}

async function savePosts(relevantPosts, companyName) {
	const dataFileName = './data/' + companyName + '.json';
	await fsWritefilePromise(dataFileName, JSON.stringify(relevantPosts));
}

run().then(function() {
	console.log('😺 says meow.');
});


//feed-shared-text-view